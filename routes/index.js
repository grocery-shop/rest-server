'use strict';

import express from 'express';
import routeNotFound from '../middlewares/404';
import * as Categories from './categories';
import * as Products from './products';

const routeParamsRegex = new RegExp('/:([^/]+)', 'g');
let router = express.Router();

addRoute('/categories/', Categories);
addRoute('/categories/:cid/products/', Products);

export default router;

function addRoute(path, ctrl) {
  let route = /\/$/.test(path) ? path : path + '/',
    routeParams = [],
    param;

  while ((param = routeParamsRegex.exec(path)) !== null) {
    routeParams.push(param[1]);
  }

  router
    .route(route)
      .get((req, res, next) => {
        addRouteMethod('list', req, res, next);
      })
      .post((req, res, next) => {
        addRouteMethod('create', req, res, next);
      });

  router
    .route(route + ':id/')
      .get((req, res, next) => {
        addRouteMethod('get', req, res, next);
      })
      .delete((req, res, next) => {
        addRouteMethod('del', req, res, next);
      })
      .put((req, res, next) => {
        addRouteMethod('update', req, res, next);
      });

  function addRouteMethod(method, req, res, next) {
    let args;
    if (ctrl[method]) {
      args = reqParams(routeParams, req)
        .concat(req.params.id, req.body);
      res.json(ctrl[method].apply(ctrl, args));
    } else {
      routeNotFound.default(req, res, next);
    }
  }
  function reqParams(params, req) {
    return params.map((p) => {
      return req.params[p];
    });
  }
}
