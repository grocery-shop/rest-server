let obj = {
  1: ['a', 'b', 'c'],
  2: ['x', 'y', 'z']
};
export function list(pid) {
  return obj[pid] || [];
}

export function create(pid, data) {
  if (!obj[pid]) {
    obj[pid] = [];
  }
  return obj[pid].push(data);
}

export function get(pid, id) {
  let o = obj[pid] || {};
  return o[id] || {};
}

export function put(pid, id, data) {
  let o = obj[pid] || {};
  o[id] = data;

  return o[id];
}

export function del(pid, id) {
  let o = obj[pid] || {};
  o.splice(id, 1);
  return {};
}
