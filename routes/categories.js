let arr = [1, 2, 3];
export function list() {
  return arr;
}

export function create(data) {
  return arr.push(data);
}

export function get(id) {
  return arr[id];
}

export function put(id, data) {
  arr[id] = data;
  return arr[id];
}

export function del(id) {
  arr.splice(id, 1);
  return {};
}
