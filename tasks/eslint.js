export let eslint = (gulp, plugins) => {
  gulp.task('eslint', () => {
    return gulp.src(['**/*.js', '!node_modules/**'])
      .pipe(plugins.eslint())
      .pipe(plugins.eslint.format())
      .pipe(plugins.eslint.failAfterError());
  });
};
