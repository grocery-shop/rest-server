'use strict';

import gulp from 'gulp';
import gulpLoadPlugins from 'gulp-load-plugins';

import * as tasks from './tasks';

const $ = gulpLoadPlugins();

Object.keys(tasks).forEach((task) => {
  tasks[task](gulp, $);
});

gulp.task('lint', ['eslint']);

gulp.task('default', () => {
  gulp.watch(['**/*.js', '!node_modules/**'], ['lint']);
});
